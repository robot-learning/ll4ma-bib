Add entries to the BibTex file for publications.
File is read to generate this webpage content: https://robot-learning.cs.utah.edu/publications
URL field defines the link for the paper title.
Use the additional "award" field in order to have extra comments on the website.